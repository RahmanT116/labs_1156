abstract class Shape {	
public abstract double Area();
}

	class Square extends Shape {
		double length;
		public double Area() {
			return length * length;
		}
	}
	
	class Circle extends Shape {
		double radius;
		double pi = 3.14;
		public double Area() {
			return pi * radius * radius;
		}
	}
	
	class Cube2 implements ThreeDShape {
		double length;
		public double Volume() {
			return 6 * length * length;
		}
	}
	
	class Sphere2 implements ThreeDShape {
		double radius;
		public double Volume() {
			return 4 * 3.14 * radius * radius;
		}
	}
		
	class TestControl{
		public static void main(String[] args) {
			double[] test1 = new double[]{2.0, 2.0};
			System.out.println("The area of the Square is " + Square(2));
			System.out.println("The area of the Circle is " + Circle(2));
		}
	}
	